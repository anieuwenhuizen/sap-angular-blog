import { SapAppPage } from './app.po';

describe('sap-app App', () => {
  let page: SapAppPage;

  beforeEach(() => {
    page = new SapAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
