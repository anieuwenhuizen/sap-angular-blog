import { Component, OnInit } from '@angular/core';
import { SapService } from './sap.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  carriers;

  constructor(private sapService: SapService) { }

  ngOnInit() {
    this.getCarriers();
  }

  getCarriers() {
    this.sapService.getCarriers().then( (data) => {
      this.carriers = data.d.results;
    } );

  }
}
