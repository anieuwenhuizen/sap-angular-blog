import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class SapService {

  private flightUrl = '/sap/opu/odata/iwfnd/rmtsampleflight/';

  constructor(private http: Http) { }

  getCarriers() {
    return this.http.get(this.flightUrl + '/CarrierCollection')
    .toPromise()
    .then((response) => {
      return response.json();
    });
  }

}
